import sys

def choose():
    
    choose = int(input("Quel outil voulez-vous utiliser ? \n1. Brute Force\n2. Scan de ports\n9. Menu précédent\n0. Quitter\n"))

    if choose == 1 :
        sys.path.append('./Bruteforce')
        import Brute

    if choose == 2 :
        sys.path.append('./Scan')
        import Scan_port_full

    if choose == 9 :
        exec(open("Menu_AS.py").read())
        
    if choose == 0 :
        sys.exit()

boucle = True
          
while boucle == True: # Tant que la 'reponse' est nulle
    choose()  
    if choose in (1,2,0):
        boucle = False
