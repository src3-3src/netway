import sys

def choose():
    
    choose = int(input("Quel outil voulez-vous utiliser ? \n1. Gestion FTP\n2. Gestion User\n3. Boite à outils\n0. Quitter\n"))

    if choose == 1 :
        sys.path.append('./FTP')
        import FTP_Menu.py
        exec(open("./FTP_Menu.py").read())

    if choose == 2 :
        exec(open("./Users/Menu_User_AS.py").read())

    if choose == 3 :
        import Menu_Boite.py
        exec(open("Menu_Boite.py").read())

    if choose == 0 :
        sys.exit()
           
    else :
        print('Valeur incorrect')

boucl = True

while boucl == True: # Tant que la 'reponse' est nulle
    choose()
    if choose in (1,2,3,0):
        boucl = False
