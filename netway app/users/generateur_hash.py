import random
import hashlib


def passwordGen():
    
    file = open('./password.txt', 'a')
    
    #Création des variables
    lettres = "abcdefghjkmnpqrstuvwxyz"
    MAJ = "ABCDEFGHJKLMNPQRSTUVWXYZ"
    chiffres = "1234567890"
    speciaux = "!@#$%&?+"

    ##Création du préfixe du mot de passe
    prefixe = random.choice(MAJ) + random.choice(lettres) + random.choice(lettres)

    ##Construction du mot de passe
    password = prefixe
    password += random.choice(speciaux)
    password += random.choice(chiffres)
    password += random.choice(chiffres)
    password += random.choice(chiffres)
    password += random.choice(speciaux)
    file.write("\n")
    file.write(password)
    empreinte = hashlib.md5(password.encode('utf-8')).hexdigest()
    return empreinte
    file.close()

passwordGen()


